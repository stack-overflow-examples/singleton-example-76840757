https://stackoverflow.com/questions/76840757/how-to-make-button-event-trigger-call-methods-from-my-instantiated-objects

Ran with UnityEngine version `2022.3.6f1`.

## Info

See `ButtonScript.cs`, and `SingletonScript.cs`.

> Methods in `ButtonScript.cs` are hooked up to the 2 buttons in the scene through the inspector.