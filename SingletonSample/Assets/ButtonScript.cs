using UnityEngine;
using UnityEngine.UI;

public class ButtonScript : MonoBehaviour
{
    [SerializeField]
    private GameObject objectPrefab;

    // Both methods attached to button in inspector...
    public void CreateNewObject()
    {
        // Calculations to ensure that the new object's position is somewhere
        // visible in the game world (in view of camera).
        Camera mainCamera = Camera.main;
        float cameraHeight = 2f * mainCamera.orthographicSize;
        float cameraWidth = cameraHeight * mainCamera.aspect;
        float randomX = Random.Range(mainCamera.transform.position.x - cameraWidth / 2f, mainCamera.transform.position.x + cameraWidth / 2f);
        float randomY = Random.Range(mainCamera.transform.position.y - cameraHeight / 2f, mainCamera.transform.position.y + cameraHeight / 2f);
        Vector3 randomPosition = new Vector3(randomX, randomY, 0f);


        GameObject newObj = Instantiate(objectPrefab, randomPosition, Quaternion.identity);
        // Singleton will hold the reference to this new object in it's list
        SingletonScript.Instance.AddToCache(newObj);
    }

    public void DestroyAllObjects()
    {
        SingletonScript.Instance.DeleteAllObjects();
    }
}
