using System.Collections.Generic;
using UnityEngine;

public class SingletonScript : MonoBehaviour
{
    private static SingletonScript instance;
    public static SingletonScript Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<SingletonScript>();
            }
            return instance;
        }
    }

    // List to hold all of the created objects.
    private List<GameObject> objectCache = new List<GameObject>();

    public void AddToCache(GameObject newObject)
    {
        objectCache.Add(newObject);
    }

    public void DeleteAllObjects()
    {
        // Delete all the GameObjects held in cache from the scene,
        // then clear the reference list,
        foreach (GameObject obj in objectCache)
        {
            Destroy(obj);
        }
        objectCache.Clear();
    }
}
